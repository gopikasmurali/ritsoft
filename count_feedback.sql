SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `count_feedback`;
CREATE TABLE `count_feedback` (
  `classid` varchar(10) NOT NULL,
  `op1` varchar(10) NOT NULL,
  `op2` varchar(10) NOT NULL,
  `op3` varchar(10) NOT NULL,
  `op4` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
