SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `online_feedback`;
CREATE TABLE `online_feedback` (
  `deptname` varchar(50) NOT NULL,
  `semid` varchar(5) NOT NULL,
  `subjectid` varchar(20) NOT NULL,
  `acdyear` varchar(10) NOT NULL,
  `responseid` int(10) NOT NULL AUTO_INCREMENT,
  `q1` varchar(5) NOT NULL,
  `q2` varchar(5) NOT NULL,
  `q21` varchar(5) NOT NULL,
  `q22` varchar(5) NOT NULL,
  `q3` varchar(5) NOT NULL,
  `fid` varchar(20) NOT NULL,
  `classid` varchar(20) NOT NULL,
  PRIMARY KEY (`responseid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
