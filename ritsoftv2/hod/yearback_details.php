<?php
include("includes/header.php");
include("includes/sidenav.php");
include("yearback_fetch.php");
?>

<style>
table
{
	font-size:16px;
 	width: 480px;
 	border-collapse: separate;
	border-spacing: 0 15px;
 	

}
table.inner
{
	border:10px;
	
}
input[type=text],select
{
	width:80%;
	padding:6px 12px;
	margin:5px 0;
	box-sizing: border-box;
}
input[type=submit]
{
	background-color: #3498DB ;
	width:50%;
	padding:8px 12px;
	margin:5px 0;
	box-sizing: border-box;	
}


</style>
<div align="center">
<form name="form1" action="yearback_changesem.php" method="post" onsubmit="validate()"; >
<table>
	<tr>
		<td>Admission No:</td> &nbsp;
		<td><input type="text" name="admo" value="<?php  echo $admisno; ?> " required readonly /></td>
    </tr>
    <tr>
    	<td>Name :</td> 
    	<td><input type="text" name="name" value="<?php  echo $name; ?> " required readonly/> </td>
    </tr>
    <tr>
    	<td>Course :</td> 
    	<td><input type="text" name="course" value="<?php  echo $course; ?> " required readonly /> </td>
    </tr>
    <tr>
    	<td>Branch :</td> 
    	<td><input type="text" name="branch" value="<?php  echo $branch; ?> " required readonly /> </td>
    </tr>
    <tr>
    	<td>Semester : </td> 
    	<td><input type="text" name="sem" value="<?php  echo $sem; ?> " required readonly /></td>
    </tr>
    <tr>
    	<td>New Sem : </td> 
    	<td>
    		<select name= "newsem" required>
				<option value="">select semester</option>
				<?php
				for ($i=1; $i < $sem; $i++)
				{
				echo "<option value=".$i."> Semester ".$i."</option>";
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
	<td> <input type="submit" name="submit" id="submit" value="submit"></td>
	</tr>
</table>
</form>
</div>

<?php
include("includes/footer.php");
?>

