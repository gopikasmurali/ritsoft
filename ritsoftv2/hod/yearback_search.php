<?php
include("includes/header.php");
include("includes/sidenav.php");
include("includes/connection.php");
?>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    
 //......................................................................   
    $('#ad_no').on("keyup input", function(){
  /* Get input value on change */
  var add_no = $(this).val();
  var resultDropdown = $(this).siblings(".result");
  if(add_no.length){
    $.get("yearback_backend.php", {add_no: add_no}).done(function(data){
                // Display the returned data in browser
                resultDropdown.html(data);
              });
  } else{
    resultDropdown.empty();
  }
});
 $(document).on("click", ".result .ad_no", function(){
  $(this).parents(".search-box").find('#ad_no').val($(this).text());
  $(this).parent(".result").empty();
});
 $(document).on("click", ".result .ad_no", function(){
  
   var add_no =$('#ad_no').val();
   
   $.ajax({
   
     type: "POST",
     url: "yearback_backend2.php",
     data: "add_no="+add_no,
     cache: false,
     success: function(result){

      $('#student-details').html(result) ;
      document.getElementById("student-details").innerHTML = result;

    }
  });
 });




//......................................................................................



//......................................................................   
    $('#name').on("keyup input", function(){
        /* Get input value on change */
        var name = $(this).val();
        var resultDropdown = $(this).siblings(".result");
        if(name.length){
            $.get("yearback_backend.php", {name: name}).done(function(data){
                // Display the returned data in browser
                resultDropdown.html(data);
            });
        } else{
            resultDropdown.empty();
        }
    });
    $(document).on("click", " .result .name", function(){
        $(this).parents(".search-box").find('#name').val($(this).text());
        $(this).parent(".result").empty();
   });
     $(document).on("click", ".result .name", function(){
        
         var name =$('#name').val();
          
            $.ajax({
             type: "POST",
             url: "yearback_backend2.php",
             data: "name="+name,
             cache: false,
             success: function(result){

            $('#student-details').html(result) ;
            document.getElementById("student-details").innerHTML = result;

}
});
});
});

</script>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><center>
                      <span style="font-weight:bold;font-size: 35px;font-family: serif;" >YEARBACK</span></center> 
                    </h1>
                    <h2 class="page-header">
                      <span style="font-weight:bold;font-size: 30px;font-family: sans-serif;">SELECT STUDENT </span>
                    </h2>
                </div>
                <!-- /.col-lg-12 -->
            </div>
 
    <!--............................................................-->
    <div class="search-box">
	
    <strong>Search by Admission No: </strong> <input type="text"  class="form-control" autocomplete="off" placeholder="Add No" id="ad_no"/>
        <div class="result"></div>
    </div>
    <!--............................................................-->
    
    <!--............................................................-->

   <div class="search-box">
      <strong>Search by Name:</strong>  <input type="text"  class="form-control" autocomplete="off" placeholder="Name" id="name"/>
        <div class="result"></div>
    </div>
    
    <div id="student-details"></div>

<form method="post" action="yearback_details.php">
<label>
  <!input type="submit" name="btn" id="btn" value="" />
  </label>
  </form>
  </div>
<?php

include("includes/footer.php");
?>